FROM ubuntu:14.04.3

MAINTAINER Krzysztof Korycki kr.korycki@gmail.com

RUN apt-get -y update && \
    apt-get install -y \
    supervisor nginx

ADD ./logs /root/logs

RUN mkdir -p /var/log/supervisor

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

#####
# NGINX
#####
ADD config/nginx.conf /opt/etc/nginx.conf
ADD config/.htpasswd /etc/nginx/.htpasswd
ADD config/public.conf /etc/nginx/sites-available/public.conf
RUN ln -s /etc/nginx/sites-available/public.conf /etc/nginx/sites-enabled/public.conf && \
    rm /etc/nginx/sites-enabled/default


ADD config/nginx-start.sh /opt/bin/nginx-start.sh
RUN chmod u=rwx /opt/bin/nginx-start.sh

ADD ./ /root/app
EXPOSE 80 22
CMD ["/usr/bin/supervisord"]